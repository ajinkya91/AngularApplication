export class User {
    id: number;
    username: string;
    password: string;
    fullname: string;
    address: string;
    city: string;
}

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { User } from '../model/user';
@Injectable()
export class UserService {
  userDetails = [];
  newUser:User;
  userFromDB =User[100];
  constructor(){
    this.getUsers();
    }
ngOnInit (){
  this.getUsers();
}
  

saveUser(user:User): any{
 this.userDetails.push(user);
 console.log(user);
localStorage.setItem('userDetails',JSON.stringify(this.userDetails));

return this.userDetails;

}

 deleteUser(user:User):any{
   let index = this.userDetails.indexOf(user);
    if (index !== -1) {
        this.userDetails.splice(index, 1);
    }
   localStorage.setItem('userDetails',JSON.stringify(this.userDetails));
   return this.userDetails;
  }


  updateUser(user:User,index:number):any{
    this.userFromDB = JSON.parse(localStorage.getItem('userDetails'));
    console.log(this.userFromDB[index]);
    this.userFromDB[index].fullname = user.fullname;
    this.userFromDB[index].address = user.address;
    this.userFromDB[index].city = user.city;
     localStorage.setItem('userDetails',JSON.stringify(this.userFromDB));
    return this.userFromDB;

  }

getUsers(){
 this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
   return this.userDetails;
}

}

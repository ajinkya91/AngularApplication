import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'create-user',
    templateUrl: './create_user.component.html',
     styles: [`
    form { margin-top: 10px; }
    label { display: block; }
    .form-group { margin-top: 5px; }
    em {color:#E05C65; padding-left:10px;}
    .error input {background-color:#E3C3C5;}

  `]
})


export class CreateUserComponent {
model: any = {};
data: any;
index:number;
edit:boolean =true;


constructor(private userService: UserService, private router: Router) {
        this.userService.getUsers();
    }

    ngOnInit (){
  this.getUsers();
}
     saveUser(user) {
    this.edit =true;
   this.data = this.userService.saveUser(user);
  }
    cancel() {
    this.router.navigate(['/login']);
    }
    delete(user){
        this.data = this.userService.deleteUser(user);
    }
    editUser(user,ndx){
        this.edit =false;
        console.log(ndx);
        this.index = ndx;
        this.model.fullname = user.fullname;
         this.model.address = user.address;
          this.model.city = user.city;
    }
    getUsers(){
        this.data = this.userService.getUsers();
    }
    updateUser(user){
        console.log(user);
        this.edit =true;
         this.data = this.userService.updateUser(user,this.index);
    }
}
